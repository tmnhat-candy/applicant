package com.candy.applicant.service.reactive;

import com.candy.applicant.dto.ApplicantDTO;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class ApplicantRxService {

    public Mono<ApplicantDTO> getApplicantMono() {

        ApplicantDTO applicant = ApplicantDTO.builder()
                .id("1")
                .name("applicant1")
                .build();

        return Mono.just(applicant);
    }

}
