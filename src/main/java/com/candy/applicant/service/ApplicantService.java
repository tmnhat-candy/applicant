package com.candy.applicant.service;

import com.candy.applicant.dto.ApplicantDTO;
import org.springframework.stereotype.Service;

@Service
public class ApplicantService {

    public ApplicantDTO getApplicant() {

        return ApplicantDTO.builder()
                .id("1")
                .name("applicant1")
                .build();
    }
}
