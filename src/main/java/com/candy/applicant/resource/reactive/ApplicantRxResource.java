package com.candy.applicant.resource.reactive;

import com.candy.applicant.dto.ApplicantDTO;
import com.candy.applicant.service.reactive.ApplicantRxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/applicant/reactive")
public class ApplicantRxResource {

    @Autowired
    ApplicantRxService applicantService;

    @GetMapping("/get")
    public ResponseEntity<Mono<ApplicantDTO>> getApplicant() {
        return ResponseEntity.ok(applicantService.getApplicantMono());
    }
}
