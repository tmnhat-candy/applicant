package com.candy.applicant.resource;

import com.candy.applicant.dto.ApplicantDTO;
import com.candy.applicant.service.ApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/applicant")
public class ApplicantResource {

    @Autowired
    ApplicantService applicantService;

    @GetMapping("/get")
    public ResponseEntity<ApplicantDTO> getApplicant() {
        return ResponseEntity.ok(applicantService.getApplicant());
    }
}
